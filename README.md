# `System.Memory` assembly bug

This is a reproduction of the bug described [here](https://developercommunity.visualstudio.com/t/visual-studio-2019-version-1610-could-not-load-fil/1434561?from=email&viewtype=all) with a minimal project.

The project was generated using [Windows Template Studio](https://marketplace.visualstudio.com/items?itemName=WASTeamAccount.WindowsTemplateStudio) with the default parameters (MVVM Toolkit and the navbar stuff).

[NLog](https://nlog-project.org/) was then added to the main project, and some initialization code has been added in [App.xaml.cs](/DebugUwp2/App.xaml.cs#L37).

Finally, a Windows application packaging project has been added with the main project in its dependencies.

Running the packaging project in `x64 Debug` mode crashes with the following info :

```
System.IO.FileLoadException
  HResult=0x80131040
  Message=Could not load file or assembly 'System.Memory, Version=4.1.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'. The located assembly's manifest definition does not match the assembly reference. (Exception from HRESULT: 0x80131040)
  Source=System.IO.FileSystem
  Arborescence des appels de procédure :
   at System.IO.DirectoryInfo.Init(String originalPath, String fullPath, String fileName, Boolean isNormalized)
   at NLog.Internal.AssemblyHelpers.GetAssemblyFileLocation(Assembly assembly)
   at NLog.Config.ConfigurationItemFactory.<GetAutoLoadingFileLocations>d__67.MoveNext()
   at NLog.Config.ConfigurationItemFactory.BuildDefaultFactory()
   at NLog.Config.ConfigurationItemFactory.get_Default()
   at NLog.Targets.TargetWithLayout..ctor()
   at NLog.Targets.DebuggerTarget..ctor()
   at DebugUwp2.App.<OnLaunched>d__4.MoveNext() in C:\Users\XXX\DebugUwp2\DebugUwp2\App.xaml.cs:line 39
```
